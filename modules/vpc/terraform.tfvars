# AWS Region
aws_region = "ap-south-1"

# VPC Configuration
vpc_cidr_block = "10.0.0.0/16"
vpc_name       = "tcs-dev-vpc"

# Subnets
public_subnet_cidrs = ["10.0.1.0/24", "10.0.2.0/24"]
private_subnet_cidrs = ["10.0.3.0/24", "10.0.4.0/24"]

# Availability Zones (Match your region)
availability_zones = ["ap-south-1a"]

# Enable DNS support
enable_dns_support = true
enable_dns_hostnames = true

# Internet Gateway
create_internet_gateway = true

# NAT Gateway
create_nat_gateway = true

# Tags
tags = {
  Environment = "Development"
  Project     = "TCSProject"
}

