terraform {
  backend "remote" {
    organization = "TCS_DIS-1"
    workspaces {
      name = "terraform-test"
    }
  }
}
