module "vpc" {
  source     = "../../modules/vpc"
  cidr_block = "10.0.0.0/16"
  vpc_name   = "dev-vpc"
}

module "subnet" {
  source            = "../../modules/subnet"
  vpc_id            = module.vpc.vpc_id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "ap-south-1a"
  subnet_name       = "dev-subnet"
}

module "ec2" {
  source        = "../../modules/ec2"
  ami           = "ami-04b4f1a9cf54c11d0"
  instance_type = "t2.micro"
  subnet_id     = module.subnet.subnet_id
  key_name      = "cloud"
  instance_name = "dev-ec2"
}

module "s3" {
  source      = "../../modules/s3"
  bucket_name = "tcs-dhadj-bucket"
}
